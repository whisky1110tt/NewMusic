import React, {useCallback, useState, useMemo} from 'react';
import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {themes, ThemeContext, TMode} from 'config/Theme';
import {StatusBar} from 'react-native';
import RootStack from 'nav/RootStack';
import {Provider} from 'react-redux';
import {store} from 'redux/reducer';
const App = () => {
  const [mode, setMode] = useState<TMode>('dark');

  const toggleTheme = useCallback(() => {
    setMode((prevMode: string) => (prevMode === 'light' ? 'dark' : 'light'));
  }, []);

  const theme = useMemo(
    () => (mode === 'dark' ? themes.dark : themes.light),
    [mode],
  );

  return (
    <Provider store={store}>
      <ThemeContext.Provider value={{theme, toggleTheme}}>
        <StatusBar
          barStyle={mode === 'dark' ? 'light-content' : 'dark-content'}
        />
        <NavigationContainer
          theme={{
            dark: true,
            colors: {
              ...DefaultTheme.colors,
              background: theme.background,
            },
          }}>
          <RootStack />
        </NavigationContainer>
      </ThemeContext.Provider>
    </Provider>
  );
};

const NewMusicApp = App;

export default NewMusicApp;
