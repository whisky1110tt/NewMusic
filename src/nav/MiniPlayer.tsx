import Icon from 'components/Icon';
import Text from 'components/Text';
import {themes, useTheme} from 'config/Theme';
import React, {
  memo,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {Dimensions, StyleSheet, View, TouchableOpacity} from 'react-native';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {ActivityIndicator, Avatar} from 'react-native-paper';
import TrackPlayer, {State} from 'react-native-track-player';
import {useSelector} from 'react-redux';
import {RootState} from 'redux/reducer';
import {usePlaybackState} from 'react-native-track-player';
import {setSongIndex} from 'redux/actions/trackplayerAction';
import Animated, {
  Easing,
  Extrapolate,
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withRepeat,
  withTiming,
} from 'react-native-reanimated';
import TextTicker from 'react-native-text-ticker';
import {openModalPlayer} from 'redux/actions/modalAction';
import {width} from 'utils/accordingDevice';

const tabHeight = getBottomSpace() + 50;
// const bottomTabHeight=useBottomTabBarHeight()

const MiniPlayer = memo(() => {
  const {theme} = useTheme();
  const playbackState = usePlaybackState();
  const [isPlaying, setIsPlaying] = useState('playing');
  const trackplayer = useSelector((state: RootState) => {
    return state.trackplayer;
  });

  const closePlayerAnimation = useSharedValue(-width); // ban đầu đang là 0
  const {list, index} = trackplayer;

  const returnPlayBtn = () => {
    switch (isPlaying) {
      case 'playing':
        return <Icon name="pause" size="big" />;
      case 'paused':
        return <Icon name="circledPlay" size="big" />;
      default:
        return <ActivityIndicator size={45} />;
    }
  };

  //reanimated 2
  const animationPlayer = useSharedValue(0);
  const closePlayerStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateX: withTiming(
            closePlayerAnimation.value,
            {
              duration: 400,
            },
            () => {
              animationPlayer.value = 2;
            },
          ),
        },
      ],
    };
  });
  // reanimated Avatar spin
  const stateAnimation = useRef(0);
  const animation = useSharedValue(stateAnimation.current);
  const rotateImgAvatar = useCallback(() => {
    animation.value = withRepeat(
      withTiming(stateAnimation.current + 1, {
        duration: 10000,
        easing: Easing.linear,
      }),
      0,
    );
  }, [animation]);
  const styleAniImg = useAnimatedStyle(() => {
    let rotateImg = interpolate(
      animation.value,
      [stateAnimation.current, stateAnimation.current + 1],
      [stateAnimation.current * 360, (stateAnimation.current + 2) * 360],
      Extrapolate.CLAMP,
    );

    return {
      transform: [{rotate: `${rotateImg}deg`}],
    };
  }, [animation.value, stateAnimation.current]);

  //get current song
  const currentSong = useMemo(
    () => trackplayer.list[trackplayer.index],
    [trackplayer],
  );
  //control player
  const onPlayPause = () => {
    if (isPlaying === 'playing' || playbackState === 3) {
      TrackPlayer.pause();
    } else if (isPlaying === 'paused' || playbackState === 2) {
      TrackPlayer.play();
    }
  };

  const onNext = () => {
    if (index < list.length - 1) {
      setSongIndex(index + 1);
    }
    if (index >= list.length - 1) {
      setSongIndex(0);
    }

    setIsPlaying('paused');
    TrackPlayer.play();
    stateAnimation.current = animation.value;
    animation.value = animation.value;
  };

  const pauseRotate = useCallback(() => {
    setIsPlaying('paused');
    stateAnimation.current = animation.value;
    animation.value = animation.value;
  }, [animation]);

  useEffect(() => {
    // set the player state

    if (playbackState === State.Playing || playbackState === 3) {
      setIsPlaying('playing');
      rotateImgAvatar();
      return;
    }
    if (playbackState === State.Ready) {
    }
    if (playbackState !== State.Playing || playbackState !== 3) {
      if (isPlaying === 'playing') {
        pauseRotate();
      }
      return;
    }
    setIsPlaying('loading');
  }, [animation, playbackState, rotateImgAvatar, pauseRotate, isPlaying]);

  const timeout = useRef<NodeJS.Timeout>();
  useEffect(() => {
    if (playbackState === State.Ready || playbackState === 3) {
      timeout.current && clearTimeout(timeout.current);
      timeout.current = setTimeout(() => {
        closePlayerAnimation.value = 1;
      }, 1000);
    }
  }, [closePlayerAnimation, playbackState]);
  return list.length > 0 ? (
    <Animated.View
      style={[
        closePlayerStyle,
        styles.container,
        {
          borderColor: theme.active_Color,
          backgroundColor: theme.background,
        },
      ]}>
      <TouchableOpacity
        style={styles.touch}
        activeOpacity={0.7}
        onPress={() => {
          openModalPlayer();
        }}>
        <Animated.View style={[styles.image, styleAniImg]}>
          <Avatar.Image source={{uri: currentSong?.artwork}} />
        </Animated.View>
        <View style={styles.viewText}>
          <TextTicker
            duration={10000}
            bounce
            repeatSpacer={50}
            marqueeDelay={0}
            style={[styles.text, {color: theme.text}]}>
            {currentSong?.title}
          </TextTicker>

          <Text size="h5" status="text_placeholder">
            {currentSong?.artist}
          </Text>
        </View>
        <TouchableOpacity onPress={onPlayPause}>
          {returnPlayBtn()}
        </TouchableOpacity>
        <TouchableOpacity onPress={onNext}>
          <Icon name="next" size="big" />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            closePlayerAnimation.value = -width;
            TrackPlayer.stop();

            setTimeout(() => {
              // resetList();
              TrackPlayer.reset();
            }, 400);
          }}>
          <Icon name="close" size="small" />
        </TouchableOpacity>
      </TouchableOpacity>
    </Animated.View>
  ) : null;
});
export default MiniPlayer;
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: tabHeight - 5,
    left: 0,
    right: 0,
    zIndex: 100,
    width: Dimensions.get('window').width,
    height: tabHeight,
    borderWidth: 2,
    borderTopColor: '#2DE9C6',
    borderBottomColor: '#DD6D5F',
    borderEndColor: '#AFE2E3',
    borderStartColor: '#AFE2E3',
    justifyContent: 'center',
  },
  touch: {
    flexDirection: 'row',
    height: tabHeight,
    paddingHorizontal: 4,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexBasis: 70,
  },
  image: {
    borderColor: themes.dark.active_Color,
    borderWidth: 1,
    borderRadius: 50,
  },
  text: {
    textTransform: 'capitalize',
    marginVertical: 4,
    fontSize: 24,
  },
  viewText: {
    width: '35%',
  },
  centeredModal: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
