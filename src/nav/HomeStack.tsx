import {createStackNavigator} from '@react-navigation/stack';
import Routes from 'config/Routes';
import React, {memo} from 'react';
import Home from 'screen/Home';

const Stack = createStackNavigator();
const HomeStack = memo(() => {
  return (
    <Stack.Navigator
      initialRouteName={'HomeStack'}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name={Routes.Home} component={Home} />

    </Stack.Navigator>
  );
});
export default HomeStack;
