import React, {memo} from 'react';
import {Drawer} from 'react-native-paper';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import Layout from 'components/Layout';
import Icon from 'components/Icon';
import Text from 'components/Text';
import t from 'utils/i18n';
import Routes from 'config/Routes';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {useTheme} from 'config/Theme';

const CustomDrawerContent = memo((props: any) => {
  const {toggleTheme} = useTheme();

  return (
    <Layout style={styles.container}>
      <DrawerContentScrollView {...props}>
        <Drawer.Section>
          <View style={styles.buttonHeader}>
            <TouchableOpacity onPress={() => props.navigation.closeDrawer()}>
              <Icon name={'close'} size="medium" />
            </TouchableOpacity>
            <TouchableOpacity onPress={toggleTheme}>
              <Icon name={'moon'} size="big" />
            </TouchableOpacity>
          </View>
          <DrawerItem
            icon={({color, size}) => <Icon name="profile" size={'medium'} />}
            label={() => <Text size="h2">{t('profile')}</Text>}
            onPress={() => {
              props.navigation.navigate('PlayerTest');
            }}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="globeEarth1" size={'medium'} />
            )}
            label={() => <Text size="h2">{t('language')}</Text>}
            onPress={() => {
              props.navigation.navigate(Routes.ChangeLanguage);
            }}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="message_text" size={'medium'} />
            )}
            label={() => <Text size="h2">{t('contact_us')}</Text>}
            onPress={() => {
              props.navigation.navigate('Home');
            }}
          />
          <DrawerItem
            icon={({color, size}) => <Icon name="Vector" size={'medium'} />}
            label={() => <Text size="h2">FAQs</Text>}
            onPress={() => {
              props.navigation.navigate('Home');
            }}
          />
          <DrawerItem
            icon={({color, size}) => <Icon name="cog" size={'medium'} />}
            label={() => <Text size="h2">{t('settings')}</Text>}
            onPress={() => {
              props.navigation.navigate(Routes.SettingsScreen);
            }}
          />
        </Drawer.Section>
      </DrawerContentScrollView>
    </Layout>
  );
});
export default CustomDrawerContent;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 19,
    marginTop: 12,
  },
});
