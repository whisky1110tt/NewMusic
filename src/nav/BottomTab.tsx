import React, {memo} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LikedSong from 'screen/LikedSongs';
import Settings from 'screen/Settings';
import {StyleSheet, View} from 'react-native';
import Icon from 'components/Icon';
import {useTheme} from 'config/Theme';
import t from 'utils/i18n';
import Home from 'screen/Home';
import {RootState} from 'redux/reducer';
import {useSelector} from 'react-redux';
import MiniPlayer from './MiniPlayer';

const Tab = createBottomTabNavigator();
const BottomTab = memo(() => {
  const {list, index} = useSelector((state: RootState) => state.trackplayer);

  const {theme} = useTheme();
  return (
    <View style={styles.container}>
      <MiniPlayer />
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: '#2DE9C6',
          inactiveTintColor: theme.inactive_Color.toString(),

          style: [{backgroundColor: theme.backgroundTab}],
        }}>
        <Tab.Screen
          name={t('home')}
          component={Home}
          options={{
            tabBarIcon: ({color}) => (
              <Icon color={color} size={'large'} name={'home'} />
            ),
          }}
        />
        <Tab.Screen
          name={t('liked_song')}
          component={LikedSong}
          options={{
            tabBarIcon: ({color}) => (
              <Icon color={color} size={'large'} name={'heart'} />
            ),
          }}
        />
        <Tab.Screen
          name={t('settings')}
          component={Settings}
          options={{
            tabBarIcon: ({color}) => (
              <Icon color={color} size={'large'} name={'gear'} />
            ),
          }}
        />
      </Tab.Navigator>
    </View>
  );
});
export default BottomTab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  icons: {
    width: 35,
    height: 35,
  },
});
