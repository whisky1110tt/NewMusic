import React, {memo, useCallback, useEffect} from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import ChangeTheme from 'screen/Settings/ChangeTheme';
import BottomTab from './BottomTab';
import ChangeLanguage from 'screen/Settings/ChangeLanguage';
import LikedSongs from 'screen/LikedSongs';
import SearchScreen from 'screen/Home/SearchScreen';
import SettingsScreen from 'screen/Settings';
import Home from 'screen/Home';
import PlayerTest from 'screen/PlayingTest/PlayerScreen';
import {useSelector} from 'react-redux';
import {RootState} from 'redux/reducer';
import TrackPlayer, {Capability, Event} from 'react-native-track-player';
import {StyleSheet, View} from 'react-native';
import PlayerScreen from 'screen/PlayingTest/PlayerScreen';
import {closeModalPlayer} from 'redux/actions/modalAction';
import {SongInfoType} from 'type/SongInfoType';
import {setSongIndex} from 'redux/actions/trackplayerAction';
type RootStackParamList = {
  Home: undefined;
  BottomTab: undefined;
  ChangeLanguage: undefined;
  ChangeTheme: undefined;
  LikedSongs: undefined;
  SearchScreen: undefined;
  SettingsScreen: undefined;
  PlayerTest: undefined;
};
const Stack = createStackNavigator<RootStackParamList>();

const RootStack = memo(() => {
  const trackplayer = useSelector((state: RootState) => {
    return state.trackplayer;
  });
  const setupTrackPlayer = useCallback(() => {
    TrackPlayer.setupPlayer().then(async () => {
      TrackPlayer.addEventListener(Event.RemoteSeek, async position => {
        await TrackPlayer.seekTo(position);
      });
      TrackPlayer.addEventListener(Event.PlaybackTrackChanged, async data => {
        setSongIndex(data.nextTrack);
      });
      TrackPlayer.updateOptions({
        stopWithApp: true,
        capabilities: [
          Capability.Play,
          Capability.Pause,
          Capability.SkipToNext,
          Capability.SkipToPrevious,
          Capability.SeekTo,
          Capability.Stop,
        ],
        compactCapabilities: [
          Capability.Play,
          Capability.Pause,
          Capability.SeekTo,
          Capability.SkipToNext,
          Capability.SkipToPrevious,
          Capability.Stop,
        ],
      });
    });
  }, []);

  const setSong = async (list: SongInfoType[]) => {
    await TrackPlayer.reset();
    await TrackPlayer.add(list);

    await TrackPlayer.skip(trackplayer.index);
    TrackPlayer.play();
  };

  useEffect(() => {
    setupTrackPlayer();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (trackplayer.list.length > 0) {
      setSong(trackplayer.list);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [trackplayer.list]);

  return (
    <View style={styles.container}>
      <Stack.Navigator
        initialRouteName={'BottomTab'}
        screenOptions={{
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS,
        }}>
        <Stack.Screen name={'Home'} component={Home} />
        <Stack.Screen name={'BottomTab'} component={BottomTab} />
        <Stack.Screen name={'ChangeLanguage'} component={ChangeLanguage} />
        <Stack.Screen name={'ChangeTheme'} component={ChangeTheme} />
        <Stack.Screen name={'LikedSongs'} component={LikedSongs} />
        <Stack.Screen name={'SearchScreen'} component={SearchScreen} />
        <Stack.Screen name={'SettingsScreen'} component={SettingsScreen} />
        <Stack.Screen name={'PlayerTest'} component={PlayerTest} />
      </Stack.Navigator>
      <PlayerScreen closeModal={closeModalPlayer} />
    </View>
  );
});
export default RootStack;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
