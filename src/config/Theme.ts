import Colors from './Color';
import React, {useContext} from 'react';
import {ColorValue} from 'react-native';

export type TMode = 'dark' | 'light';

export interface ITheme {
  slider_color: ColorValue;
  background: ColorValue;
  text: ColorValue;
  text_placeholder: ColorValue;
  active_Color: ColorValue;
  backgroundTab: ColorValue;
  inactive_Color: ColorValue;
  iconColor: ColorValue;
}
export interface IThemeContext {
  theme: ITheme;
  toggleTheme: () => void;
}

export const themes = {
  dark: {
    background: Colors.primary,
    text: Colors.color_basic_1,
    text_placeholder: Colors.color_basic_3_7,
    active_Color: Colors.color_basic_5,
    backgroundTab: Colors.color_basic_2,
    inactive_Color: Colors.color_basic_1,
    iconColor: Colors.color_basic_1,
    slider_color: Colors.color_basic_4_6,
  },
  light: {
    background: Colors.secondary,
    text: Colors.color_basic_2,
    text_placeholder: Colors.color_basic_3,
    active_Color: Colors.color_basic_5,
    backgroundTab: Colors.color_basic_2,
    inactive_Color: Colors.color_basic_1,
    iconColor: Colors.primary,
    slider_color: Colors.color_basic_4_6,
  },
};

export const ThemeContext = React.createContext({
  theme: themes.light,
  toggleTheme: () => {},
});

export const useTheme = (): IThemeContext => {
  const {theme, toggleTheme} = useContext(ThemeContext);
  return {
    theme,
    toggleTheme,
  };
};
