const Colors = {
    primary:'#091227',
    secondary:'#F7FAFF',
    color_basic_1:'#F7FAFF',
    color_basic_2:'#000000',
    color_basic_3:'rgb(165,192,255)',
    color_basic_3_7:'rgba(165,192,255,.7)',
    color_basic_4:'#8996B8',
    color_basic_4_6:'rgba(137,150,184,.6)',
    color_basic_5:'rgb(14, 174, 14)',
  };
  
  export default Colors;
  