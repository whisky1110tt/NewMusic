import t from 'utils/i18n'
const Routes = {
  MainTab: 'MainTab',
  Home: t('home'),
  ChangeTheme: 'ChangeTheme',
  LikedSongs: t('liked_song'),
  SettingsScreen: 'SettingsScreen',
  BottomTab: 'BottomTab',
  PlayingNow: t('playing_now'),
  ChangeLanguage: t('en'),
  SingerInfo: 'SingerInfo',
  DrawerSettings: 'DrawerSettings',
  ProfileScreen: 'ProfileScreen',
  SearchScreen: 'SearchScreen',
  PlayerTest: 'PlayerTest',
};
export default Routes;
