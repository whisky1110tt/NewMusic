export interface SongInfoType {
  title: string;
  artist: string;
  artwork: string;
  url: string;
  id: number;
}
