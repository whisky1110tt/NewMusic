export enum EnumSongType {
  'Suggest' = 10,
  'Trending' = 20,
  'Top' = 30,
  'V-Pop' = 40,
}
