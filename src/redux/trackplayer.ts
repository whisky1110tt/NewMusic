export const initVale = {
  list: [],
  index: null,
  isReady: false,
  play: false,
  currentPosition: null,
};

export const ADD_LIST = 'ADD_LIST';
export const NEXT_SONG = 'NEXT_SONG';
export const RESET = 'RESET';

const trackplayer = (state = initVale, action: any) => {
  switch (action.type) {
    case ADD_LIST:
      return {
        ...state,
        list: action.list,
        index: action.index,
        play: true,
      };
    case NEXT_SONG:
      return {
        ...state,
        list: state.list,
        index: action.index,
      };
    case RESET:
      return {
        ...initVale,
      };

    default:
      return state;
  }
};
export default trackplayer;

// trackPlayer action
export const setSong = ({list, index}: {list: []; index: number}) => ({
  type: NEXT_SONG,
  list,
  index,
});

export function addList({list, index}: {list: []; index: number}) {
  return {
    type: ADD_LIST,
    list,
    index,
  };
}

export function resetTrackPlayer() {
  return {
    type: RESET,
  };
}
