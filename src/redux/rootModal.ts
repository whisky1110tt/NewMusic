const rootValue = {
  visible: false,
};

const rootModal = (state = rootValue, action: any) => {
  switch (action.type) {
    case 'SHOW_POPUP':
      return {
        visible: true,
      };
    case 'CLOSE_POPUP':
      return rootValue;
    default:
      return state;
  }
};

export default rootModal;
