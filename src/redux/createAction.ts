//ACTIONS
export const createActions = (type: string, data: any) => ({
  type,
  ...data,
});
