import {AnyAction} from 'redux';

export const initVale = {
  list: [],
  loading: false,
};

const searchResult = (state = initVale, action: AnyAction) => {
  switch (action.type) {
    case 'LOADING':
      return {
        ...state,
        loading: true,
      };
    case 'LOAD_DONE':
      return {
        ...state,
        loading: false,
      };
    case 'FECTH_DATA':
      return {
        ...state,
        list: [...action.data.list],
      };
    case 'LOAD_MORE':
      return {
        ...state,
        list: state.list.concat(action.data.list),
      };
    case 'RESET':
      return {
        list: initVale.list,
      };

    default:
      return state;
  }
};
1;
export default searchResult;
