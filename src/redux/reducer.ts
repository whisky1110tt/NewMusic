import {applyMiddleware, combineReducers, createStore} from 'redux';
import trackplayer from './trackplayer';
import rootModal from './rootModal';
import thunk from 'redux-thunk';
import searchResult from './searchResults';
import createSagaMiddleware from 'redux-saga';
import {watcherSaga} from './sagas/rootSaga';

const rootReducer = combineReducers({
  trackplayer,
  rootModal,
  searchResult,
});

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware, thunk];

// export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
export const store = createStore(
  rootReducer,
  {},
  applyMiddleware(...middlewares),
);
sagaMiddleware.run(watcherSaga);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
