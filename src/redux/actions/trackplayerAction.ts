import {SongInfoType} from 'type/SongInfoType';
import {store} from './../reducer';
import {createActions} from './../createAction';

export const addList = (list: SongInfoType[], index: number) => {
  const action = createActions('ADD_LIST', {list, index});
  store.dispatch(action);
};

export const setSongIndex = (index: number | string) => {
  const action = createActions('NEXT_SONG', {index});
  store.dispatch(action);
};
export const resetList = () => {
  const action = createActions('RESET', {});
  store.dispatch(action);
};
