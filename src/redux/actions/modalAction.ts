import {createActions} from 'redux/createAction';
import {store} from 'redux/reducer';

export const closeModalPlayer = () => {
  const action = createActions('CLOSE_POPUP', {visible:false});
  store.dispatch(action);
};
export const openModalPlayer = () => {
  const action = createActions('SHOW_POPUP', {visible: true});
  store.dispatch(action);
};
//ok