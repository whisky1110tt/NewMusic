import {AppDispatch, RootState} from 'redux/reducer';
import {postAxios} from 'utils/axios';

const getData = (list: any[]) => {
  return {
    type: 'FECTH_DATA',
    data: {
      list,
    },
  };
};

const loading = () => {
  return {
    type: 'LOADING',
  };
};
const loadDone = () => {
  return {
    type: 'LOAD_DONE',
  };
};

export const fecthData = () => {
  return function (dispatch: AppDispatch, getState: () => RootState) {
    console.log('getState', getState());
    dispatch(loading());
    postAxios('http://localhost:8080/api/songs/filter', {
      data: {
        filterModel: {},
        pageSize: 2,
        pageIndex: 1,
      },
    }).then((res: any) => {
      console.log('data', res.data.data.docs);
      dispatch(loadDone());
      if (res.status !== 200) {
        return;
      }
      dispatch(getData(res.data.data.docs));
    });
  };
};
