import axios from 'axios';
import {Alert} from 'react-native';

interface AxiosResponseType {
  data: any;
  statusText: string;
  status: number;
}

export const getAxios = (
  url: string,
  params: any,
): Promise<AxiosResponseType> => {
  return new Promise((resolve, reject) => {
    axios
      .get(url, {
        params,
      })
      .then(response => {
        resolve({
          data: response.data,
          statusText: response.statusText,
          status: response.status,
        });
      })
      .catch(error => {
        console.log(error);
        Alert.alert('Thông báo', JSON.stringify(error.message));
        reject(error);
      });
  });
};

export function postAxios(url: string, body: any) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, body)
      .then(response => {
        resolve({
          data: response.data,
          statusText: response.statusText,
          status: response.status,
        });
      })
      .catch(error => {
        console.log(error);
        Alert.alert('Thông báo', JSON.stringify(error.message));
        reject(error);
      });
  });
}
