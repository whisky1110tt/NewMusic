import Icon from 'components/Icon';
import Layout from 'components/Layout';
import {useTheme} from 'config/Theme';
import React, {useState} from 'react';
import {memo} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import TrackPlayer, {State, usePlaybackState} from 'react-native-track-player';
export const Controller = memo(() => {
  const playbackState = usePlaybackState();
  const {theme} = useTheme();
  const onNext = () => {
    TrackPlayer.skipToNext();
  };
  const onPrv = () => {
    TrackPlayer.skipToPrevious();
  };

  const PauseNPlay = () => {
    if (playbackState === State.Playing || playbackState === 3) {
      TrackPlayer.pause();
    }
    if (playbackState === State.Paused || playbackState === 2) {
      TrackPlayer.play();
    }
  };
  const PauseNPlayBtn = () => {
    switch (playbackState) {
      case State.Paused:
        return <Icon name="circledPlay" size="huge" />;
      case State.Playing:
        return <Icon name="pause" size="huge" />;
      default:
        return <ActivityIndicator size={55} />;
    }
  };
  return (
    <Layout style={styles.container}>
      <TouchableOpacity
        onPress={onPrv}
        activeOpacity={0.7}
        style={styles.buttonNextNPrv}>
        <Icon name={'back'} size="big" />
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.7} onPress={PauseNPlay}>
        {PauseNPlayBtn()}
      </TouchableOpacity>
      <TouchableOpacity
        onPress={onNext}
        activeOpacity={0.7}
        style={styles.buttonNextNPrv}>
        <Icon name={'next'} size="big" />
      </TouchableOpacity>
    </Layout>
  );
});
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 8,
  },
  playNPause: {
    width: 100,
    height: 100,
  },
  buttonNextNPrv: {
    marginTop: 12,
  },
});
