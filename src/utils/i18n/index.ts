import i18n from 'i18n-js';
import en from './en.json';
import vi from './vi.json';


export const availableTranslations = {
  en,
  vi,
};



i18n.translations = availableTranslations;
i18n.locale='vi'
i18n.fallbacks = true;
export const setLocale = (language: string) => {
  i18n.locale = language;
};

// const i18n = require('i18n');
// i18n.configure({
//   locales: ['vi','en'],
//   defaultLocale: 'en',
//   updateFiles: false,
//   directory: './locales'
// });

// i18n.setLocale('en');
export default (key: string, config?: i18n.TranslateOptions | undefined) =>
  i18n.translate(key, config);

