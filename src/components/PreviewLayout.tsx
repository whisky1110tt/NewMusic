import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
interface PreviewLayoutProps {
  label: string;
  children: any;
}
const PreviewLayout = ({label, children}: PreviewLayoutProps) => (
  <View style={{padding: 10, flex: 1}}>
    <Text style={styles.label}>{label}</Text>

    <View style={[styles.container, {[label]: 'wrap'}]}>{children}</View>
  </View>
);

export default PreviewLayout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 8,
  },
  label: {
    textAlign: 'center',
    marginBottom: 10,
    fontSize: 24,
  },
});
