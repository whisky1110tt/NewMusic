import React from 'react';
import {Text as DefaultText, TextStyle, TextProps} from 'react-native';
import {useTheme, ITheme} from 'config/Theme';

interface Props extends TextProps {
  children?: string;
  style?: TextStyle;
  status?: keyof ITheme;
  size?: TSize;
  type?: 'bold' | 'medium' | 'regular' | 'semiBold';
  font?: string;
  marginBottom?: number;
}

export type TSize = 'h1' | 'h2' | 'h3' | 'h4' | 'h5';

export const BaseFont = '';

export const Fonts = {
  Bold: 'SFProText-Bold',
  Medium: `SFProText-Medium`,
  Semibold: `SFProText-Semibold`,
  Light: `SFProText-Light`,
  Regular: `SFProText-Regular`,
  Heavy: `SFProText-Heavy`,
};

const Size = {
  h1: {
    value: 24,
    lineHeight: 26,
  },
  h2: {
    value: 20,
    lineHeight: 22,
  },
  h3: {
    value: 18,
    lineHeight: 20,
  },
  h4: {
    value: 16,
    lineHeight: 18,
  },
  h5: {
    value: 14,
    lineHeight: 16,
  },
  h6: {
    value: 10,
    lineHeight: 12,
  },
};
const Text = ({
  status = 'text',
  size = 'h1',
  font = Fonts.Regular,
  type,
  marginBottom,
  ...props
}: Props) => {
  const {theme} = useTheme();

  return (
    <DefaultText
      numberOfLines={1}
      {...props}
      style={[
        {
          color: theme[status],
          fontSize: Size[size].value,
          fontFamily: font,
          lineHeight: Size[size].lineHeight,
          marginBottom: marginBottom,
          textTransform: 'capitalize',
        },
        props.style,
      ]}>
      {props.children}
    </DefaultText>
  );
};

export default Text;
