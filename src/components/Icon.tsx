import {useTheme} from 'config/Theme';
import React from 'react';
import {Image, ImageStyle, StyleSheet} from 'react-native';
import Icons from 'assets/Icon';

interface IconProps {
  name?: string;
  size?: 'small' | 'medium' | 'large' | 'big' | 'huge';
  color?: string;
  style?: ImageStyle;
}

enum Size {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
  BIG = 'big',
  Huge = 'huge',
}

const Icon = ({name = 'back', size, color, style}: IconProps) => {
  const {theme} = useTheme();

  return (
    <Image
      source={Icons[`${name}`]}
      style={[
        style,
        {
          tintColor: color ? color : theme.iconColor,
        },
        size === Size.SMALL
          ? styles.small
          : size === Size.LARGE
          ? styles.large
          : size === Size.BIG
          ? styles.big
          : size === Size.Huge
          ? styles.huge
          : styles.medium,
      ]}
    />
  );
};
export default Icon;
const styles = StyleSheet.create({
  small: {
    width: 20,
    height: 20,
  },
  medium: {
    width: 28,
    height: 28,
  },
  large: {
    width: 32,
    height: 32,
  },
  big: {
    width: 50,
    height: 50,
  },
  huge: {
    width: 65,
    height: 65,
  },
});
