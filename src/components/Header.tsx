import React, {memo} from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import Icon from './Icon';
import Layout from './Layout';
import Text from './Text';
interface Props {
  title?: string;
  iconLeft?: string;
  iconRight?: string;
  onLeft?(): void;
  onRight?(): void;
}

const Header = memo(({title, iconLeft, iconRight, onLeft, onRight}: Props) => {
  return (
    <Layout style={styles.container}>
      <TouchableOpacity activeOpacity={0.7} onPress={onLeft}>
        <Icon style={styles.button} size={'large'} name={iconLeft} />
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.text} size="h1" status="text" type={'bold'}>
          {title}
        </Text>
      </View>
      {!!iconRight && (
        <TouchableOpacity
          onPress={onRight}
          activeOpacity={0.7}
          style={styles.button}>
          <Icon style={styles.button} size={'large'} name={iconRight} />
        </TouchableOpacity>
      )}
    </Layout>
  );
});
export default Header;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 24,
    paddingTop: 32 + 12,
    paddingBottom: 8,
  },
  text: {
    justifyContent: 'center',
    textAlign: 'center',
    marginRight: 32,
  },
  title: {
    flex: 1,
    height: 28,
    alignItems: 'center',
  },
  button: {justifyContent: 'center'},
});
