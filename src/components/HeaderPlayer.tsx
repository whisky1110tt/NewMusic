import React, {memo} from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import Icon from './Icon';
import Layout from './Layout';
import Text from './Text';
interface Props {
  title?: string;
  iconLeft?: string;
  iconRight?: string;
  onLeft?(): void;
  onRight?(): void;
}

const HeaderPlayer = memo(
  ({title, iconLeft, iconRight, onLeft, onRight}: Props) => {
    return (
      <Layout style={styles.container}>
        <TouchableOpacity activeOpacity={0.7} onPress={onLeft}>
          <Icon size={'large'} name={iconLeft} />
        </TouchableOpacity>
        <View style={styles.title}>
          <Text style={styles.text} size="h1" status="text" type={'bold'}>
            {title}
          </Text>
        </View>
        {!!iconRight && (
          <TouchableOpacity onPress={onRight} activeOpacity={0.64}>
            <Icon size={'large'} name={iconRight} />
          </TouchableOpacity>
        )}
      </Layout>
    );
  },
);
export default HeaderPlayer;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 24,
    paddingTop: 32 + 12,
    paddingBottom: 8,
    justifyContent: 'space-between',
  },
  text: {
    justifyContent: 'center',
    textAlign: 'center',
  },
  title: {
    height: 28,
    alignItems: 'center',
  },
});
