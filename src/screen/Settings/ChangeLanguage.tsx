import React from 'react';
import Layout from 'components/Layout';
import Text from 'components/Text';
import {Dimensions, StyleSheet, TouchableOpacity, View} from 'react-native';
import t, {setLocale} from 'utils/i18n';
import Header from 'components/Header';
import {useNavigation} from '@react-navigation/native';
const width = Dimensions.get('window').width - 30;
const ChangeLanguage = () => {
  const navigation = useNavigation();

  return (
    <Layout>
      <Header
        title={t('language')}
        iconLeft={'backArrow'}
        onLeft={() => navigation.goBack()}
      />
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            setLocale('vi');
            navigation.goBack();
          }}
          style={styles.vi}>
          <Text>{t('vi')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setLocale('en');
            navigation.goBack();
          }}
          style={styles.en}>
          <Text>{t('en')}</Text>
        </TouchableOpacity>
      </View>
    </Layout>
  );
};
export default ChangeLanguage;
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  vi: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: width,
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 10,
    borderTopColor: '#2DE9C6',
    borderBottomColor: '#DD6D5F',
    borderEndColor: '#AFE2E3',
    borderStartColor: '#AFE2E3',
  },
  en: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: width,
    borderWidth: 1,
    borderRadius: 10,
    borderTopColor: '#2DE9C6',
    borderBottomColor: '#DD6D5F',
    borderEndColor: '#AFE2E3',
    borderStartColor: '#AFE2E3',
  },
});
