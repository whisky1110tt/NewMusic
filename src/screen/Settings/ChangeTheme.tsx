import React, {memo} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Layout from 'components/Layout';
import {useTheme} from 'config/Theme';
import Text, {Fonts} from 'components/Text';
import Header from 'components/Header';
import t from 'utils/i18n';
import {useNavigation} from '@react-navigation/native';
const ChangeTheme = memo(() => {
  const {theme, toggleTheme} = useTheme();
  console.log('theme', theme);
  const navigation = useNavigation();
  return (
    <Layout style={styles.container}>
      <Header
        title={t('display')}
        iconLeft={'backArrow'}
        onLeft={() => navigation.goBack()}
      />

      <TouchableOpacity onPress={toggleTheme} style={styles.button}>
        <Text font={Fonts.Bold} status="text" size="h1" style={styles.text}>
          Change Mode
        </Text>
      </TouchableOpacity>
    </Layout>
  );
});
export default ChangeTheme;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    justifyContent: 'center',
    textAlign: 'center',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 10,
    borderTopColor: '#2DE9C6',
    borderBottomColor: '#DD6D5F',
    borderEndColor: '#AFE2E3',
    borderStartColor: '#AFE2E3',
    marginHorizontal: 24,
  },
});
