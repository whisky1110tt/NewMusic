import {useNavigation} from '@react-navigation/native';
import React, {memo} from 'react';
import Routes from 'config/Routes';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Layout from 'components/Layout';
import Text, {Fonts} from 'components/Text';
import Icon from 'components/Icon';
import t from 'utils/i18n';
import Header from 'components/Header';

interface OptionsArrProps {
  title?: any;
  image?: string;
  route?: any;
}
const OptionsArr = [
  {
    title: 'liked_song',
    image: 'heart1',
    route: Routes.LikedSongs,
  },
  {
    title: 'language',
    image: 'globeEarth',
    route: Routes.ChangeLanguage,
  },
  {
    title: 'settings_theme',
    image: 'gear',
    route: Routes.ChangeTheme,
  },
];
const SettingsScreen = memo(() => {
  const navigation = useNavigation();

  return (
    <Layout style={styles.container}>
      <Header
        iconLeft={'backArrow'}
        title={t('settings')}
        onLeft={() => navigation.goBack()}
      />

      {OptionsArr.map((item: OptionsArrProps, index) => {
        return (
          <TouchableOpacity
            key={index}
            onPress={() => navigation.navigate(item.route)}
            style={styles.itemStyle}>
            <Icon size={'large'} name={item.image} style={styles.icon} />
            <Text
              font={Fonts.Bold}
              status="text"
              size="h5"
              style={styles.textTitle}>
              {t(item.title)}
            </Text>
          </TouchableOpacity>
        );
      })}
    </Layout>
  );
});

export default SettingsScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 100,
  },
  icon: {
    marginLeft: 16,
  },
  textTitle: {
    fontSize: 18,
    lineHeight: 20,
    marginLeft: 14,
    alignSelf: 'center',
  },
  itemStyle: {
    flexDirection: 'row',
    marginTop: 20,
    marginLeft: 16,
    marginRight: 16,
    borderRadius: 10,
    borderWidth: 1,
    height: 40,
    alignItems: 'center',
    borderTopColor: '#2DE9C6',
    borderBottomColor: '#DD6D5F',
    borderEndColor: '#AFE2E3',
    borderStartColor: '#AFE2E3',
  },
  textHeader: {
    fontSize: 24,
    lineHeight: 26,
    alignSelf: 'center',
    marginTop: 32,
  },
});
