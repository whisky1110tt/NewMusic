import Layout from 'components/Layout';
import React, {memo, useRef} from 'react';
import {Dimensions} from 'react-native';
import YouTube from 'react-native-youtube';
import YoutubePlayer from 'react-native-youtube-iframe'
import scaleAccordingToDevice from 'utils/accordingDevice';

interface Props {
  videoId: string;
  isPlay: boolean;
  isMute?: boolean;
  videoRef: any;
}
const widthIMG = scaleAccordingToDevice(245, true);
const widthD = Dimensions.get('window').width;

const VideoPlayer = memo(({videoId, isPlay, videoRef}: Props) => {
  return (
    <Layout>
      <YoutubePlayer
        height={widthIMG}
        videoId={videoId}
        ref={videoRef}
        play={isPlay}
      />
      {/* <YouTube
        apiKey={'AIzaSyCnKKYgMydpQVRnGOsUoKgOX8h7bXovn7s'}
        videoId={videoId}
        play={isPlay}
        style={{height: 500}}
        onChangeFullscreen={() => true}
        onError={e => console.log('youtube error->', e)}
        autoPlay
      /> */}
    </Layout>
  );
});
export default VideoPlayer;
