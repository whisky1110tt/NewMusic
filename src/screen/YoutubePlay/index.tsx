import React, {memo, useEffect, useRef, useState} from 'react';
import Text, {Fonts} from 'components/Text';
import Layout from 'components/Layout';
import {SafeAreaView, StyleSheet, TouchableOpacity, View} from 'react-native';
import Icon from 'components/Icon';
import Slider from '@react-native-community/slider';
import t from 'utils/i18n';
import {useNavigation, useRoute} from '@react-navigation/native';
import Routes from 'config/Routes';
import ItemPlayer from './itemPlayer';
import VideoPlayer from './videoPlayer';
const PlayingNow = memo(() => {
  const [isPauseAndPlay, setIsPauseAndPlay] = useState(true);
  const [isMuted, setIsMuted] = useState(false);
  const [currentTimes, setCurrentTimes] = useState(0);
  const route = useRoute();
  const videoRef = useRef<any>();
  const navigation = useNavigation();
  useEffect(() => {}, [videoRef]);
  return (
    <Layout style={{flex: 1}}>
      <SafeAreaView>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => navigation.navigate(Routes.BottomTab)}>
            <Icon size={'big'} name={'backArrow'} color={''} />
          </TouchableOpacity>
          <View style={{flex: 1}}>
            <Text
              status="text"
              size="h1"
              font={Fonts.Bold}
              style={styles.textHeader}>
              {t('playing_now')}
            </Text>
          </View>
        </View>

        <VideoPlayer
          videoRef={videoRef}
          videoId={route?.params?.videoId}
          isPlay={isPauseAndPlay}
        />
        {/* <VideoPlayer
            videoId={route?.params?.videoId}
            isPlay={isPauseAndPlay}
          /> */}
        <ItemPlayer
          nameSong={route?.params?.nameSong}
          channelTitle={route?.params?.channelTitle}
        />

        <View style={styles.option}>
          <TouchableOpacity onPress={() => setIsMuted(!isMuted)}>
            {isMuted === true ? (
              <Icon size="big" name={'noAudio'} />
            ) : (
              <Icon size="big" name={'volume'} />
            )}
          </TouchableOpacity>
          <View style={styles.buttonPlayer}>
            <TouchableOpacity>
              <Icon size="big" name={'repeat'} />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon size="big" name={'outline'} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.timePlayer}>
          <Text>{currentTimes.toString()}</Text>
          <Text></Text>
        </View>

        <Slider
          minimumValue={0}
          maximumValue={100}
          minimumTrackTintColor="#FFF"
          maximumTrackTintColor="#555B6A"
          onSlidingStart={() => {
            videoRef.current.getCurrentTime();
          }}
          onSlidingComplete={() => {}}
          style={styles.slider}
        />

        <View style={styles.buttonPlayer}>
          <TouchableOpacity>
            <Icon size={'huge'} name="back" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setIsPauseAndPlay(!isPauseAndPlay)}>
            {isPauseAndPlay === true ? (
              <Icon size="huge" name="pause" />
            ) : (
              <Icon size="huge" name="circledPlay" />
            )}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Icon size="huge" name="next" />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </Layout>
  );
});
export default PlayingNow;
const styles = StyleSheet.create({
  option: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 36,
    marginHorizontal: 28,
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    paddingHorizontal: 24,
    paddingVertical: 20,
  },
  buttonPlayer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 80,
    marginTop: 40,
  },
  slider: {
    width: '95%',
    height: 50,
    alignSelf: 'center',
    marginTop: 36,
  },
  timePlayer: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 30,
  },
  textHeader: {
    textAlign: 'center',
    justifyContent: 'center',
  },
});
