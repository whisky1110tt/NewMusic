import React, {memo} from 'react';
import Text, {Fonts} from 'components/Text';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import Icon from 'components/Icon';
import TextTicker from 'react-native-text-ticker';
import {useTheme} from 'config/Theme';
interface itemPlayerProps {
  channelTitle: string;
  nameSong: string;
  onPress?(): () => {};
}
const ItemPlayer = memo(
  ({onPress, nameSong, channelTitle}: itemPlayerProps) => {
    const {theme} = useTheme();
    return (
      <View style={styles.container}>
        <View style={{paddingHorizontal: 24}}>
          <TextTicker
            style={[styles.textTicker, {color: theme.iconColor}]}
            duration={15000}
            loop
            bounce
            repeatSpacer={100}
            marqueeDelay={1000}>
            {nameSong}
          </TextTicker>
          <Text
            style={styles.channelTitle}
            size="h4"
            status="text_placeholder"
            font={Fonts.Regular}>
            {channelTitle}
          </Text>
        </View>
        <TouchableOpacity onPress={onPress}>
          <Icon size="large" name={'heart'} style={styles.iconHeart} />
        </TouchableOpacity>
      </View>
    );
  },
);
export default ItemPlayer;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameSong: {
    textAlign: 'center',
    marginTop: 28,
    justifyContent: 'center',
    marginHorizontal: 12,
  },
  channelTitle: {
    textAlign: 'center',
    marginTop: 12,
    justifyContent: 'center',
  },
  iconHeart: {
    position: 'absolute',
    justifyContent: 'center',
    marginLeft: 90,
  },
  textTicker: {
    fontSize: 24,
    lineHeight: 28,
    marginTop: 12,
    fontFamily: Fonts.Medium,
  },
});
