import Text from 'components/Text';
import React from 'react';
import {TouchableOpacity, Image, StyleSheet, ViewStyle} from 'react-native';
import scaleAccordingToDevice from 'utils/accordingDevice';
interface Props {
  artwork: string;
  title?: string;
  artist?: string;
  style?: ViewStyle;
  index: number;
  onPress?(): void;
}

const widthIMG = scaleAccordingToDevice(144);
const heightItem = scaleAccordingToDevice(200);

const ItemSong = ({artwork, title, artist, index, style, onPress}: Props) => (
  <TouchableOpacity
    activeOpacity={0.7}
    onPress={onPress}
    style={[styles.container, {marginRight: index % 2 === 0 ? 18 : 0}, style]}>
    <Image source={{uri: artwork}} style={styles.image} />
    <Text size="h3" status="text" style={styles.text}>
      {title}
    </Text>
    <Text size="h5" status="text_placeholder" style={styles.textPlaceholder}>
      {artist}
    </Text>
  </TouchableOpacity>
);
export default ItemSong;

const styles = StyleSheet.create({
  container: {
    width: widthIMG,
    height: heightItem,
    borderRadius: 5,
    overflow: 'hidden',
    marginLeft: 4,
  },
  image: {
    width: '100%',
    height: widthIMG,
  },
  text: {
    alignSelf: 'center',
    marginTop: 12,
    fontFamily: 'SFProText-Regular',
    textTransform: 'capitalize',
  },
  textPlaceholder: {
    alignSelf: 'center',
    marginTop: 6,
    fontFamily: 'SFProText-Regular',
    textTransform: 'capitalize',
  },
});
