import React, {memo, useCallback} from 'react';
import Text, {Fonts} from 'components/Text';
import Layout from 'components/Layout';
import {StyleSheet, FlatList} from 'react-native';
import ItemSong from './ItemSong';
import Header from 'components/Header';
import {useNavigation} from '@react-navigation/native';
import {NavigationStackProp} from 'react-navigation-stack';
import t from 'utils/i18n';
import songs from 'screen/PlayingTest/data';
import {addList} from 'redux/actions/trackplayerAction';
import {openModalPlayer} from 'redux/actions/modalAction';

const LikedSongs = memo(() => {
  const navigation = useNavigation<NavigationStackProp>();
  const renderItem = useCallback(
    ({item, index}) => (
      <ItemSong
        style={styles.item}
        artwork={item.artwork}
        title={item.title}
        artist={item.artist}
        index={index}
        onPress={() => {
          openModalPlayer();
          addList(songs, index);
        }}
      />
    ),
    [],
  );

  return (
    <Layout style={styles.container}>
      <Header
        onLeft={() => navigation.openDrawer()}
        iconLeft={'menu'}
        onRight={() => navigation.navigate('SearchScreen')}
        iconRight={'search'}
      />
      <Text font={Fonts.Bold} status="text" size="h1" style={styles.header}>
        {t('liked_songs')}
      </Text>
      <FlatList
        data={songs}
        renderItem={renderItem}
        numColumns={2}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.id.toString()}
        contentContainerStyle={styles.contentContainerStyle}
      />
    </Layout>
  );
});

export default LikedSongs;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    marginTop: 24,
    marginLeft: 28,
    marginBottom: 8,
  },
  contentContainerStyle: {
    paddingHorizontal: 27,
    paddingBottom: 85,
  },
  item: {
    marginTop: 24,
  },
});
