import React, {memo, useEffect, useState} from 'react';
import Text from 'components/Text';
import {ScrollView, StyleSheet} from 'react-native';
import Layout from 'components/Layout';
import HomeMusicItem from './HomeMusicItem';
import {addList} from 'redux/actions/trackplayerAction';
import {SongInfoType} from 'type/SongInfoType';
import {openModalPlayer} from 'redux/actions/modalAction';
import {getAxios} from 'utils/axios';
import {EnumSongType} from 'type/EnumSongType';
interface Props {
  titleList: string;
}

const TrendingSrc = memo(({titleList}: Props) => {
  const [loading, setLoading] = useState(false);
  const [listSearch, setListSearch] = useState<SongInfoType[]>([]);
  console.log('loading', loading);

  useEffect(() => {
    setLoading(true);
    getAxios('https://60bee5ce320dac0017be412f.mockapi.io/api/song/song', {
      type: EnumSongType.Trending,
    }).then(res => {
      if (res.status !== 200) {
        setLoading(false);
        return;
      }
      setListSearch(res.data.items);
      setLoading(false);
    });
  }, []);
  return (
    <Layout style={styles.container}>
      <Text size="h1" status="text" style={styles.title}>
        {titleList}
      </Text>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {listSearch.map((item, index) => {
          return (
            <HomeMusicItem
              title={item.title}
              artist={item.artist}
              key={index}
              artwork={item.artwork}
              onPress={() => {
                openModalPlayer();
                addList(listSearch, index);
              }}
            />
          );
        })}
      </ScrollView>
    </Layout>
  );
});
export default TrendingSrc;
const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
  },
  title: {
    marginBottom: 20,
    marginLeft: 28,
    fontFamily: 'SFProText-Regular',
  },
});
