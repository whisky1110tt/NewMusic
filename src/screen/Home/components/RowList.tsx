import React, {memo} from 'react';
import Text from 'components/Text';
import {ScrollView, StyleSheet} from 'react-native';
import Layout from 'components/Layout';
import HomeMusicItem from './HomeMusicItem';
import songs from 'screen/PlayingTest/data';
import {addList} from 'redux/actions/trackplayerAction';
import {SongInfoType} from 'type/SongInfoType';
import {openModalPlayer} from 'redux/actions/modalAction';
interface Props {
  titleList: string;
  listSongs?: SongInfoType[];
}

const RowList = memo(({titleList}: Props) => {
  return (
    <Layout style={styles.container}>
      <Text size="h1" status="text" style={styles.title}>
        {titleList}
      </Text>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {songs.map((item, index) => {
          return (
            <HomeMusicItem
              title={item.title}
              artist={item.artist}
              key={index}
              artwork={item.artwork}
              onPress={() => {
                openModalPlayer();
                addList(songs, index);
              }}
            />
          );
        })}
      </ScrollView>
    </Layout>
  );
});
export default RowList;
const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
  },
  title: {
    marginBottom: 20,
    marginLeft: 28,
    fontFamily: 'SFProText-Regular',
  },
});
