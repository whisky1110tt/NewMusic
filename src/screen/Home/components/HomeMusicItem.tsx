import React from 'react';
import {memo} from 'react';
import {Image, StyleSheet, TouchableOpacity} from 'react-native';
import Text from 'components/Text';
import scaleAccordingToDevice from 'utils/accordingDevice';

interface Props {
  artwork: string;
  title: string;
  artist: string;
  onPress?(): void;
  onPressSinger?(): void;
}

const HomeMusicItem = memo(
  ({artwork, title, artist, onPress, onPressSinger}: Props) => {
    return (
      <TouchableOpacity
        style={styles.item}
        activeOpacity={0.54}
        onPress={onPress}>
        <Image source={{uri: artwork}} style={styles.image} />
        <TouchableOpacity activeOpacity={0.54} style={styles.buttonSong}>
          <Text
            size="h3"
            status="text"
            style={styles.nameSong}
            numberOfLines={1}>
            {title}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={0.54} onPress={onPressSinger}>
          <Text size="h5" status="text_placeholder" style={styles.artist}>
            {artist}
          </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  },
);

export default HomeMusicItem;

const widthIMG = scaleAccordingToDevice(190);

const styles = StyleSheet.create({
  item: {
    marginHorizontal: 10,
    alignItems: 'center',
    width: widthIMG,
  },
  nameSong: {
    fontFamily: 'SFProText-Medium',
    width: widthIMG,
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  artist: {
    marginBottom: 20,
    textAlign: 'center',
    fontFamily: 'SFProText-Regular',
    textTransform: 'capitalize',
    width: widthIMG,
    marginLeft: 16,
  },
  image: {
    marginBottom: 16,
    marginLeft: 16,
    borderRadius: 5,
    width: widthIMG,
    height: widthIMG,
  },
  buttonSong: {
    marginBottom: 6,
    marginLeft: 16,
    width: widthIMG,
    alignItems: 'center',
  },
});
