import React, {memo} from 'react';
import Text from 'components/Text';
import {StyleSheet} from 'react-native';
import Layout from 'components/Layout';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Avatar} from 'react-native-paper';
interface Props {
  onPress?(): void;
  title: string;
  artist: string;
  artwork: string;
}
const SearchItem = memo(({onPress, title, artist, artwork}: Props) => {
  return (
    <Layout>
      <TouchableOpacity
        style={styles.container}
        onPress={onPress}
        activeOpacity={0.54}>
        <Avatar.Image source={{uri: artwork}} style={styles.image} size={50} />
        <Layout style={styles.textView}>
          <Text style={styles.title} size="h3" status="text">
            {title}
          </Text>
          <Text style={styles.artist} size="h4" status="text_placeholder">
            {artist}
          </Text>
        </Layout>
      </TouchableOpacity>
    </Layout>
  );
});
export default SearchItem;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 10,
    borderTopColor: '#2DE9C6',
    borderBottomColor: '#DD6D5F',
    borderEndColor: '#AFE2E3',
    borderStartColor: '#AFE2E3',
  },
  image: {
    marginLeft: 4,
  },
  title: {
    textTransform: 'capitalize',
    marginVertical: 8,
  },
  artist: {
    textTransform: 'capitalize',
    marginBottom: 4,
  },
  textView: {
    marginLeft: 24,
    justifyContent: 'center',
    width: '70%',
  },
});
