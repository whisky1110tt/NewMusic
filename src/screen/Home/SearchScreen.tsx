import {useNavigation} from '@react-navigation/native';
import Icons from 'assets/Icon';
import Icon from 'components/Icon';
import Layout from 'components/Layout';
import Text from 'components/Text';
import {themes, useTheme} from 'config/Theme';
import React, {memo, useCallback, useState} from 'react';
import {ActivityIndicator, ScrollView, StyleSheet, View} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {Avatar} from 'react-native-paper';
import {openModalPlayer} from 'redux/actions/modalAction';
import {addList} from 'redux/actions/trackplayerAction';
import {SongInfoType} from 'type/SongInfoType';
import {getAxios} from 'utils/axios';
import t from 'utils/i18n';
import SearchList from './components/SearchItem';

const SearchScreen = memo(() => {
  const {theme} = useTheme();
  const navigation = useNavigation();
  const [keySearch, setKeySearch] = useState('');
  const [loading, setLoading] = useState(false);
  const [listSearch, setListSearch] = useState<SongInfoType[]>([]);
  const onSearch = useCallback(() => {
    setLoading(true);
    getAxios('https://60bee5ce320dac0017be412f.mockapi.io/api/song/song', {
      search: keySearch,
    }).then(res => {
      if (res.status !== 200) {
        setLoading(false);
        return;
      }
      setListSearch(res.data.items);
      setLoading(false);
    });
  }, [keySearch]);
  return (
    <Layout style={styles.container}>
      <Layout style={styles.searchBar}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            navigation.goBack();
          }}>
          <Icon style={styles.button} size={'large'} name={'backArrow'} />
        </TouchableOpacity>
        <View
          style={[
            styles.textInputContainer,
            {
              backgroundColor: theme.iconColor,
            },
          ]}>
          <TextInput
            value={keySearch}
            onChangeText={setKeySearch}
            placeholder={'  ' + t('Search') + ' . . .'}
            placeholderTextColor={theme.background}
            style={styles.textInput}
          />
        </View>

        <TouchableOpacity activeOpacity={0.7} onPress={onSearch}>
          <Avatar.Icon icon={Icons.search} size={45} />
        </TouchableOpacity>
      </Layout>

      {loading ? (
        <Layout style={styles.isLoading}>
          <ActivityIndicator size="large" color={'#FFFFFF'} />
        </Layout>
      ) : (
        <Layout style={styles.resultScroll}>
          <Text size="h3" status="text" style={styles.text}>
            {t('result :')}
          </Text>
          <Text size="h3" status="text_placeholder">
            {keySearch}
          </Text>
          <ScrollView>
            {listSearch.map((item, index) => {
              return (
                <SearchList
                  title={item.title}
                  artist={item.artist}
                  key={index}
                  artwork={item.artwork}
                  onPress={() => {
                    openModalPlayer();
                    addList([item], 0);
                  }}
                />
              );
            })}
          </ScrollView>
        </Layout>
      )}
    </Layout>
  );
});

export default SearchScreen;
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    paddingTop: 24 + 36,
    paddingBottom: 8,
    flex: 1,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    justifyContent: 'center',
    marginTop: 24,
    marginBottom: 24,
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginRight: 12,
  },
  textInputContainer: {
    width: '75%',
    height: 50,
    borderRadius: 8,
    marginHorizontal: 12,
    textAlign: 'left',
    justifyContent: 'center',
  },
  isLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  resultScroll: {
    paddingBottom: 120,
  },
  textInput: {
    marginLeft: 8,
    color: themes.dark.active_Color,
  },
});
