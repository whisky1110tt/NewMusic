import Layout from 'components/Layout';
import React, {memo} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import Header from 'components/Header';
import {useNavigation} from '@react-navigation/native';
import {NavigationStackProp} from 'react-navigation-stack';
import t from 'utils/i18n';
import SuggestList from './components/SuggestList';
import TrendingSrc from './components/TrendingSrc';
const Home = memo(() => {
  const navigation = useNavigation<NavigationStackProp>();

  return (
    <Layout style={styles.container}>
      <Header
        title={''}
        iconLeft={'music'}
        iconRight={'search'}
        onRight={() => navigation.navigate('SearchScreen')}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.contentContainerStyle}>
        <SuggestList titleList={t('recommended_for_you')} />
        <TrendingSrc titleList={'Trending'} />
      </ScrollView>
    </Layout>
  );
});
export default Home;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainerStyle: {
    paddingBottom: 70,
  },
});
