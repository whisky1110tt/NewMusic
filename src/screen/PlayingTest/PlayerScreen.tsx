import React, {useRef, useEffect, useMemo, memo} from 'react';
import {View, StyleSheet, Image, Modal, Dimensions} from 'react-native';
import Layout from 'components/Layout';
import {State, usePlaybackState, useProgress} from 'react-native-track-player';
import Text from 'components/Text';
import {Controller} from 'utils/Controller/Controller';
import SliderComp from './components/SliderComp';
import {RootState} from 'redux/reducer';
import {useSelector} from 'react-redux';
import {themes, useTheme} from 'config/Theme';
import t from 'utils/i18n';
import Animated, {
  Easing,
  Extrapolate,
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withRepeat,
  withTiming,
} from 'react-native-reanimated';
import scaleAccordingToDevice from 'utils/accordingDevice';
import HeaderPlayer from 'components/HeaderPlayer';
import ControllerRepeat from './components/ControllerRepeat';
const width = Dimensions.get('window').width;
const widthIMG = scaleAccordingToDevice(260);
const height = Dimensions.get('window').height;
interface Props {
  closeModal: () => void;
}
const PlayerScreen = memo(({closeModal}: Props) => {
  const {toggleTheme} = useTheme();

  // const [isPlaying, setIsPlaying] = useState('playing'); //paused play loading
  const playbackState = usePlaybackState();
  const stateAnimation = useRef(0);
  const animation = useSharedValue(stateAnimation.current);
  const trackplayer = useSelector((state: RootState) => {
    return state.trackplayer;
  });
  const progress = useProgress();

  const {visible: visibleRoot} = useSelector(
    (state: RootState) => state.rootModal,
  );

  //reanimated 2
  const rotateImgAvatar = () => {
    animation.value = withRepeat(
      withTiming(stateAnimation.current + 1, {
        duration: 10000,
        easing: Easing.linear,
      }),
      0,
    );
  };
  const styleAniImg = useAnimatedStyle(() => {
    let rotateImg = interpolate(
      animation.value,
      [stateAnimation.current, stateAnimation.current + 5],
      [stateAnimation.current * 360, (stateAnimation.current + 10) * 360],
      Extrapolate.CLAMP,
    );

    return {
      transform: [{rotate: `${rotateImg}deg`}],
    };
  }, [animation.value, stateAnimation.current]);

  useEffect(() => {
    console.log('playbackState', playbackState);
    if (playbackState === State.Playing || playbackState === 3) {
      rotateImgAvatar();
      return;
    }
    if (playbackState !== State.Playing || playbackState !== 3) {
      stateAnimation.current = animation.value;
      animation.value = animation.value;
      return;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [animation, playbackState]);

  // Get id currentSong
  const currentSong = useMemo(
    () => trackplayer.list[trackplayer.index],
    [trackplayer],
  );

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={visibleRoot}
      animated={true}>
      <View style={styles.modalView}>
        <Layout style={styles.container}>
          <HeaderPlayer
            title={t('playing_now')}
            onRight={toggleTheme}
            iconLeft={'ic_down'}
            iconRight={'moon'}
            onLeft={closeModal}
          />
          <Animated.View style={[styles.imageView, styleAniImg]}>
            <Image source={{uri: currentSong?.artwork}} style={styles.image} />
          </Animated.View>
          <View style={styles.textView}>
            <Text size="h1" status="text" style={styles.title}>
              {currentSong?.title}
            </Text>
            <Text size="h2" status="text_placeholder">
              {currentSong?.artist}
            </Text>
          </View>
          <View style={styles.controller}>
            <ControllerRepeat />
            <SliderComp
              currentDuration={progress.duration}
              currentPosition={progress.position}
            />
            <Layout style={styles.controllerMusic}>
              <Controller />
            </Layout>
          </View>
        </Layout>
      </View>
    </Modal>
  );
});
export default PlayerScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  controllerMusic: {
    position: 'absolute',
    right: 0,
    left: 0,
    zIndex: 100,
    top: height / 4.5,
  },
  imageView: {
    marginTop: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: widthIMG,
    height: widthIMG,
    borderRadius: 200,
    borderColor: themes.dark.active_Color,
    borderWidth: 2,
  },
  title: {
    textTransform: 'capitalize',
    marginTop: 12,
  },
  artist: {
    marginTop: 6,
    textTransform: 'capitalize',
  },
  textView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  option: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 36,
    marginHorizontal: 28,
    alignItems: 'center',
  },
  controller: {
    marginTop: 32,
  },
  modalView: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    width: width,
    height: height,
  },
});
