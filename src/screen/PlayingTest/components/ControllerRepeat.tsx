import React, {useCallback, useEffect, useState} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'components/Icon';
import Layout from 'components/Layout';
import TrackPlayer, {RepeatMode} from 'react-native-track-player';
import {themes} from 'config/Theme';
interface props {
  isOutLine?: () => {};
}

const ControllerRepeat = ({isOutLine}: props) => {
  const [isRepeat, setIsRepeat] = useState(false);
  const [isMuted, setIsMuted] = useState(false);
  useEffect(() => {
    if (isMuted === true) {
      TrackPlayer.setVolume(0);
    } else {
      setIsMuted(false);
      TrackPlayer.setVolume(1);
    }
  }, [isMuted]);
  useCallback(() => {
    if (isRepeat === false) {
      TrackPlayer.setRepeatMode(RepeatMode.Off);
    } else {
      TrackPlayer.setRepeatMode(RepeatMode.Track);
    }
  }, [isRepeat]);
  return (
    <Layout style={styles.container}>
      <TouchableOpacity
        onPress={() => {
          setIsMuted(!isMuted);
        }}>
        {isMuted === false ? (
          <Icon name="volume" size="large" />
        ) : (
          <Icon name="noAudio" size="large" />
        )}
      </TouchableOpacity>
      <View style={styles.button}>
        <TouchableOpacity
          onPress={() => {
            setIsRepeat(!isRepeat);
          }}
          style={styles.onLoop}>
          {isRepeat === false ? (
            <Icon name="repeat" size="large" />
          ) : (
            <Icon name="repeat" size="large" color={themes.dark.active_Color} />
          )}
        </TouchableOpacity>
        <TouchableOpacity onPress={isOutLine}>
          <Icon name="outline" size="large" />
        </TouchableOpacity>
      </View>
    </Layout>
  );
};
export default ControllerRepeat;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 8,
  },
  button: {
    flexDirection: 'row',
  },
  onLoop: {
    marginRight: 12,
  },
});
