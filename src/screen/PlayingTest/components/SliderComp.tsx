import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import Slider from '@react-native-community/slider';
import TrackPlayers from 'react-native-track-player';
import {useTheme} from 'config/Theme';
import Text from 'components/Text';
import timeConvert from 'utils/timeConvert';
interface Props {
  currentPosition: number;
  currentDuration: number;
}
export default function SliderComp({currentPosition, currentDuration}: Props) {
  const {theme} = useTheme();
  const [isSeeking, setIsSeeking] = useState(false);
  const [seek, setSeek] = useState(0);

  const handleChange = (val: number) => {
    TrackPlayers.seekTo(val);
  };
  return (
    <View style={styles.container}>
      <View style={styles.timeContainer}>
        <Text style={styles.timers}>{timeConvert(currentPosition)}</Text>
        <Text style={styles.timers}>{timeConvert(currentDuration)}</Text>
      </View>
      <Slider
        style={styles.slider}
        minimumValue={0}
        value={currentPosition}
        onValueChange={value => {
          setIsSeeking(true);
          setSeek(value);
        }}
        maximumValue={currentDuration}
        minimumTrackTintColor={theme.text.toString()}
        onSlidingComplete={handleChange}
        maximumTrackTintColor={theme.slider_color.toString()}
        thumbTintColor={theme.text.toString()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  timers: {
    fontSize: 16,
    marginTop: 12,
  },
  timeContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 30,
    marginBottom: 8,
  },
  slider: {
    alignSelf: 'center',
    width: '90%',
  },
});
