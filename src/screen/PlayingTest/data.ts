const songs = [
  {
    title: 'death bed',
    artist: 'Powfu',
    artwork: 'https://picsum.photos/seed/picsum/200/300',
    url: 'https://sample-music.netlify.app/death%20bed.mp3',
    id: 0,
  },
  {
    title: 'bad liar',
    artist: 'Imagine Dragons',
    artwork: 'https://picsum.photos/200/300?grayscale',

    url: 'https://sample-music.netlify.app/Bad%20Liar.mp3',
    id: 1,
  },
  {
    title: 'faded',
    artist: 'Alan Walker',
    artwork: 'https://picsum.photos/id/1/200/300',
    url: 'https://sample-music.netlify.app/Faded.mp3',
    id: 2,
  },
  {
    title: 'hate me',
    artist: 'Ellie Goulding',
    artwork: 'https://picsum.photos/id/10/200/300',
    url: 'https://sample-music.netlify.app/Hate%20Me.mp3',
    id: 3,
  },
  {
    title: 'Solo',
    artist: 'Clean Bandit',
    artwork: 'https://picsum.photos/id/100/200/300',
    url: 'https://sample-music.netlify.app/Solo.mp3',
    id: 4,
  },
  {
    title: 'without me fukin idiots abcxyz terterter',
    artist: 'Halsey',
    artwork: 'https://picsum.photos/id/1000/200/300',
    url: 'https://sample-music.netlify.app/Without%20Me.mp3',
    id: 5,
  },
];

export default songs;
